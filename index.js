const express = require("express");
const app = express();

app.set("PORT", process.env.PORT || 5000);

app.get("/", (req, res) => {
    res.status(200).send({
        message: "How do I deploy my code to Heroku using GitLab CI/CD? 1"
    });
});

app.listen(app.get("PORT"), () =>
    console.log(`Server running on port ${app.get("PORT")}`)
);
